import * as fs from "fs";

class FizzBuzz {
    x: string;
    y: string;

    constructor(x: string = "Fizz", y:string = "Buzz") {
        this.x = x;
        this.y = y;
    }

    print(start: number, end: number) {
        for (let i: number = start; i <= end; i++) {
            if (i % 3 == 0) {
                if (i % 5 == 0) {
                    process.stdout.write(this.x + this.y);
                } else {
                    process.stdout.write(this.x);
                }
            } else {
                if (i % 5 == 0) {
                    process.stdout.write(this.y);
                } else {
                    process.stdout.write("" + i);
                }
            }
            if (i !== end) {
                process.stdout.write(" ");
            }
        }
        process.stdout.write("\n");
    }
}
const no_args: number = process.argv.length;
if (no_args < 3) {
	console.log("Missing arguments");
} else {
    let fb: FizzBuzz;
    const lines: string[] = fs.readFileSync(process.argv[2], 'utf8').split("\n");
    if (no_args < 4) {
        fb = new FizzBuzz();
    } else if (no_args < 5) {
        fb = new FizzBuzz(process.argv[3]);
    } else {
        fb = new FizzBuzz(process.argv[3], process.argv[4]);
    }
    for (const line of lines) {
        let pair: string[] = line.split(",");
        fb.print(parseInt(pair[0]), parseInt(pair[1]));
    }
}
